<!DOCTYPE html>
<html lang="en">
<head>
    <title>Modul 7_PHP</title>
    <style>
        body {
            padding: 10px;
            background-color: #E0A890;

        }
        h1 {
            padding: 10px;
            font-size: 20px;
            color: #065143;
            border: 2px solid #666 ;
        }
    </style>
</head>
<body>
    <h1> 
    <!-- funtion -->
    <?php
    $name = "DEVIA NUR OKTAVIANI";
    $nim = "6702213027";
    $email = "devia3012@gmail.com";
    $noHP = "085547246891";
    $noWA = "6285524805780"; //Wajib pakai kode negara (kode Indonesia = 62)
    $tglLahir = "2000-12-31";
    $umur = hitungUmur($tglLahir);

    function hitungUmur($birthdayDate){
        $date = new DateTime($birthdayDate);
        $now = new DateTime();
        $interval = $now->diff($date);
        return $interval->y;
    }

    echo "Nama Saya: ";
    echo $name;
    echo "<br>";
    echo "NIM Saya: ";
    echo $nim;
    echo "<br>";
    echo "Email Saya: ";
    echo " <a href='mailto://".$email."' target='_blank'>".$email."</a>";
    echo "<br>";
    echo "Ho HP Saya: ";
    echo " <a href='tel://".$noHP."' target='_blank'>".$noHP."</a>";
    echo "<br>";
    echo "Ho WA Saya: ";
    echo " <a href='https://wa.me/".$noWA."' target='_blank'>".$noWA."</a>";
    echo "<br>";
    echo "Tgl Lahir Saya: ";
    echo $tglLahir;
    echo "<br>";
    echo "Umur Saya: ";
    echo $umur." Tahun";
    echo "<br>";
    echo "2 Tahun yang lalu saya berumur ".($umur-2)." Tahun<br>";
    echo "Tahun depan saya akan berumur ".($umur+1)." Tahun<br>";
    echo "<a href='lingkaran.php'>Klik untuk belajar lingkaran dengan PHP</a>";
    echo "<br>";
    echo "<a href='perulangan.php'>Klik untuk belajar perulangan dengan PHP</a>";
    echo "<br>";
    echo "<a href='percabangan.php'>Klik untuk belajar percabangan dengan PHP</a>";
    echo "<br>";
    ?>

    </h1>
    
</body>
</html>