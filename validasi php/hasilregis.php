<html>
<body>
<?php
//open daftar.php
if (isset($_POST["btndftr"])){
	$nama = $_POST["txtnama"];
	$nim = $_POST["txtnim"];
	$username= $_POST["txtusername"];
    $password= $_POST["txtpassword"];
    $ulangpassword= $_POST["txtulangpassword"];
}else{
	die("Anda harus mengisi yang benar lewat form pendaftaran");
}
//masukan nama
if (!empty($nama)){
	if (!preg_match("/^[a-zA-Z\s]*$/",$nama)) {
      echo "<b>Nama hanya boleh huruf!</b><br>";
    }else{
		echo "Thanks, <b>". $nama."</b><br>";
	}
}else{
	echo("Anda belum memasukkan nama <br>");
}

//masukan nim
if (!empty($nim)){
	if (!preg_match("/^[0-9]*$/",$nim)) {
      echo "<b>NIM  hanya boleh terdiri dari angka</b><br>"; 
    }else{
		echo "NIM, <b>". $nim."</b><br>";
	}
}else{
	echo("Anda belum memasukkan nim <br>");
}

//masukkan username
if (!empty($username)){
    $lusername= strlen($username);
	if (!preg_match("/^[a-zA-Z0-9]*$/",$username)) {
      echo "<b>Username hanya boleh terdiri dari huruf</b><br>";
    }elseif ((($lusername <= 6)) || (($lusername >= 15))){
		echo ("<b>Username minimal 6 karakter dan maksimal 15 karakter</b> <br>");
    }else{
		echo "Username anda, <b>". $username."</b><br>";
	}
}else{
	echo("Anda belum memasukkan username <br>");
}

//masukkan password
if (!empty($ulangpassword)){
	if (!preg_match("/^[0-9]*$/",$password)) {
      echo "<b>Password hanya boleh terdiri dari angka</b><br>"; 
    }else{
		echo "Password, <b>". $password."</b><br>";
	}
}else{
	echo("Anda belum memasukkan password <br>");
}

//masukkan ulang password
if (!empty($password)){
	if (!preg_match("/^[0-9]*$/",$password)) {
      echo "<b>Password hanya boleh terdiri dari angka</b><br>"; 
    }else{
		echo "ulangPassword, <b>". $ulangpassword."</b><br>";
	}
// }else{
// 	echo("Password pertama dan kedua tidak sama <br>");
}

?>
</body>
</html>