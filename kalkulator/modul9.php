<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kalkulator Nilai</title>
    <style>
      * {
        box-sizing: border-box;
      }
      body {
        margin: 0;
        background: teal;
      }
      .text-center {
        text-align: center;
      }
      .box {
        margin: auto;
        /* border: 1px solid rgb(233, 233, 233); */
        padding: 15px;
        border-radius: 20px;
        display: table;
        background-color: seashell;
      }
      .btn-submit {
        width: 100%;
        margin-top: 15px;
      }
      table, th, td {
        border: 2px solid rgb(0 0 0);
        text-align: center;
        padding: 5px;
    }
    tfoot td{
        font-weight: bold;
    }
    </style>
</head>
<body>
<?php
$assessment_1 = $_POST['assessment_1'];
$assessment_2 = $_POST['assessment_2'];
$assessment_3 = $_POST['assessment_3'];
$praktikum = $_POST['praktikum'];

$pa_1 = 10;
$pa_2 = 20;
$pa_3 = 30;
$p = 40;

$ta_1 = (($assessment_1 * 10)/100);
$ta_2 = (($assessment_2 * 20)/100);
$ta_3 = (($assessment_3 * 30)/100);
$tp = (($praktikum * 40)/100);

$total = $ta_1 + $ta_2 + $ta_3 + $tp;

$grade = 'E';
if($total > 80){
    $grade = 'A';
}
else if($total <= 80 && $total > 70){
    $grade = 'AB';
}
else if($total <= 70 && $total > 65){
    $grade = 'B';
}
else if($total <= 65 && $total > 60){
    $grade = 'BC';
}
else if($total <= 60 && $total > 50){
    $grade = 'C';
}
else if($total <= 50 && $total > 40){
    $grade = 'C';
}
?>
<h1 class="text-center">Kalkulator Nilai Kuliah</h1>
<div class="box">
    <table>
        <thead>
            <tr>
                <th>Komponen</th>
                <th>Nilai</th>
                <th>Prosentase</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Assessment-1</td>
                <td><?php echo $assessment_1 ?></td>
                <td><?php echo $pa_1 ?>%</td>
                <td><?php echo $ta_1 ?></td>
            </tr>
            <tr>
                <td>Assessment-2</td>
                <td><?php echo $assessment_2 ?></td>
                <td><?php echo $pa_2 ?>%</td>
                <td><?php echo $ta_2 ?></td>
            </tr>
            <tr>
                <td>Assessment-3</td>
                <td><?php echo $assessment_3 ?></td>
                <td><?php echo $pa_3 ?>%</td>
                <td><?php echo $ta_3 ?></td>
            </tr>
            <tr>
                <td>Praktikum</td>
                <td><?php echo $praktikum ?></td>
                <td><?php echo $p ?>%</td>
                <td><?php echo $tp ?></td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3">Nilai Akhir</td>
                <td colspan="3"><?php echo $total ?></td>
            </tr>
            <tr>
                <td colspan="3">Grade</td>
                <td colspan="3"><?php echo $grade ?></td>
            </tr>
        </tfoot>
    </table>
</div>
</body>
</html>